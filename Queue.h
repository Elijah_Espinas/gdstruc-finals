#pragma once
#include "UnorderedArray.h"
#include <iostream>
#include <string>
#include <assert.h>
template <class T>

class QueueSet : public UnorderedArray<T>
{
public:
	QueueSet(int size) : UnorderedArray<T>(size) {}
private:
	UnorderedArray<T>* mContainer;
};
