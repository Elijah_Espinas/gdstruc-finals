#include <iostream>
#include <string>
#include <conio.h>
#include "Stack.h"
#include "Queue.h"

using namespace std;

int main()
{
	int inputSize, numInput;
	string userInput;

	cout << "Enter size for element sets: ";
	cin >> inputSize;

	QueueSet<int> queueArr(inputSize);
	StackSet<int> stackArr(inputSize);

	while (true)
	{
		cout << "What do you want to do?" << endl << "[1] - Push top element" << endl << "[2] - Pop top element" << endl << "[3] - Display all then empty sets" << endl;
		cin >> userInput;

		if (userInput == "1")
		{
			cout << "Enter element to be pushed: ";
			cin >> numInput;

			queueArr.push(numInput);
			cout << "Queue first element: " << queueArr.display(0) << endl;
			stackArr.push(numInput);
			cout << "Stack top lement " << stackArr.display(stackArr.getSize() - 1) << endl;
		}
		else if (userInput == "2")
		{
			cout << "Top elements have been removed" << endl << "Queue first element removed: ";
			queueArr.popFirst();
			cout << endl << "Stack top element removed: ";
			stackArr.popTop();

			cout << endl << endl << "New top elements:" << endl << "Queue: " << queueArr.display(0) << endl << "Stack: " << stackArr.display(stackArr.getSize() - 1);
		}
		else if (userInput == "3")
		{
			cout << "Queue : ";
			for (int i = 0; i < queueArr.getSize(); i++)
				cout << queueArr.display(i) << " ";
			
			cout << endl << "Stack : ";
			for (int x = 0; x < stackArr.getSize(); x++)
				cout << stackArr.display(x) << " ";

			queueArr.clearArray();
			stackArr.clearArray();
		}
		else
		{
		
		}

		cout << endl;
		system("pause");
		system("cls");
	}

	return 0;
}