#pragma once
#include "UnorderedArray.h"
#include <iostream>
#include <string>
#include <assert.h>
template <class T>

class StackSet : public UnorderedArray<T>
{
public:
	StackSet(int size) : UnorderedArray<T>(size) {}
private:
	UnorderedArray<T>* mContainer;
};
